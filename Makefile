##############################################################################
# 
# Makefile for programming assignments
# 
#    - 05/18/96 SNF: created.
#    - 02/1999  AHF: modified for CS255
#
##############################################################################

target: chat

JAVA            = java
JAVAC           = javac

JAVASRCS        = \
			Chat/*.java

JAVAOBJS        = $(JAVASRCS:.java=.class)

.SUFFIXES:	.class .java

.java.class: $*.java
	    $(JAVAC) $(JAVACFLAGS) $*.java;

clean:
	    rm -f Chat/*\$*.class Chat/*.class

chat:    $(JAVAOBJS)
