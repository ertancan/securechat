package Chat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SymmetricKeyHelper {
	
	public SymmetricKeyHelper(){
		
	}
	
	public static String encryptMessage(byte[] msg,Key secret){
		try {
        	Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			byte[] ciphertext = cipher.doFinal(msg);
			System.out.println("Encrypted" + new String(ciphertext));
			return new String(ciphertext);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "-1";
	}
	
	public static String decryptMessage(byte[] encryptedMsg, Key secret){
		try {
			
        	Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, secret);
			byte[] plaintext = cipher.doFinal(encryptedMsg);
			System.out.println("Decrypted : " + new String(plaintext));
			return new String(plaintext);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "-1";
	}
	public static String decryptMessage(String encryptedMsg, Key secret){
		return decryptMessage(encryptedMsg.getBytes(), secret);
	}
	public static String encryptMessage(String msg,Key secret){ 
		return encryptMessage(msg.getBytes(), secret);
	}
	public static void generateKeyAndWriteToFile(String filename){
		try{
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
		       kgen.init(128); 


		       // Generate the secret key specs.
		       SecretKey skey = kgen.generateKey();
			saveToFile(filename+".aes", skey);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void saveToFile(String fileName,
			Key key) throws IOException {
		ObjectOutputStream oout = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(fileName)));
		try {
			oout.writeObject(key);
		} catch (Exception e) {
			throw new IOException("Unexpected error", e);
		} finally {
			oout.close();
		}
	}
	public static Key readKeyFromFile(String filename){
		ObjectInputStream oin;
		try {
			InputStream in =new FileInputStream(filename+".aes");
			oin =
				new ObjectInputStream(new BufferedInputStream(in));
			Key k = (Key) oin.readObject();
			return k;
		} catch (Exception e) {
			throw new RuntimeException("Spurious serialisation error", e);
		}
	}
}
