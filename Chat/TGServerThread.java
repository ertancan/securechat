//
//  TGServerThread.java
//
//  Written by : Priyank Patel <pkpatel@cs.stanford.edu>
//
//  Accepts connection requests and processes them
package Chat;

// socket
import java.net.*;
import java.io.*;

// Swing
import javax.swing.JTextArea;

//  Crypto
import java.security.*;
import java.security.spec.*;
import java.security.interfaces.*;
import java.sql.Date;
import java.util.Calendar;

import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class TGServerThread extends Thread {

	private TGServer _tgs;
	private ServerSocket _serverSocket = null;
	private int _portNum;
	private String _hostName;
	private JTextArea _outputArea;

	public TGServerThread(TGServer tgs) {

		super("AuthServerThread");
		_tgs = tgs;
		_portNum = tgs.getPortNumber();
		_outputArea = tgs.getOutputArea();
		_serverSocket = null;

		try {

			InetAddress serverAddr = InetAddress.getByName(null);
			_hostName = serverAddr.getHostName();

		} catch (UnknownHostException e) {
			_hostName = "0.0.0.0";
		}
	}

	//  Accept connections and service them one at a time
	public void run() {
		try { _serverSocket = new ServerSocket(_portNum);
		_outputArea.append("AS waiting on " + _hostName + " port " + _portNum+"\n");
		while (true) {
			Socket socket = _serverSocket.accept();
			PrintWriter _out=new PrintWriter(socket.getOutputStream(),true);
			BufferedReader _in=new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			_out.println("Hello");
			String input=_in.readLine();
			if(input.startsWith("request")){
				System.out.println("room");
				String[]splited=input.split("::");
				String rest=splited[1];
				String SymEnc=AsymmetricKeyHelper.decryptMessage(new BASE64Decoder().decodeBuffer(rest), AsymmetricKeyHelper.readPrivateKeyFromFile("server"));
				String parts[]=SymEnc.split("::");
				String username=parts[0];
				System.out.println("das ist eine username:"+username);
				System.out.println("ist das ein Herrscher?:"+parts[1]);
				String TGT=SymmetricKeyHelper.decryptMessage(new BASE64Decoder().decodeBuffer(parts[1]), SymmetricKeyHelper.readKeyFromFile("server"));
				String[] tgtParts=TGT.split("::");
				System.out.println("tgt user:"+tgtParts[0]);
				if(username.equals(tgtParts[0])){ //User is correct
					System.out.println("einen richtig user");
					long tgtTime=Long.parseLong(tgtParts[1]);
					Calendar cal=Calendar.getInstance();
					if (cal.getTimeInMillis()-tgtTime<1000000){ //new tgt
						System.out.println("ja ja");
						_out.println("room"+splited[0].substring(8));
					}
				}
				
			}else{
				String SymEnc=AsymmetricKeyHelper.decryptMessage(new BASE64Decoder().decodeBuffer(input), AsymmetricKeyHelper.readPrivateKeyFromFile("server"));
				String dec=SymmetricKeyHelper.decryptMessage(SymEnc, SymmetricKeyHelper.readKeyFromFile("server"));
				String[] parts=dec.split("::");

				String username=parts[1];
				//System.out.println("username:"+username);

				Calendar cal=Calendar.getInstance();
				String TGT=username+"::"+cal.getTimeInMillis();
				_out.println(new BASE64Encoder().encode(SymmetricKeyHelper.encryptMessage(TGT, SymmetricKeyHelper.readKeyFromFile("server")).getBytes()).replaceAll("\n", ""));
				//
				//  Got the connection, now do what is required
				//  */
			}

			_out.close();
			_in.close();
			socket.close();
			//
		}
		} catch (Exception e) {
			System.out.println("AS thread error: " + e.getMessage());
			e.printStackTrace();
		}

	}
}
