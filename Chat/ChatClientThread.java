/**
 *  Created 2/16/2003 by Ting Zhang 
 *  Part of implementation of the ChatClient to receive
 *  all the messages posted to the chat room.
 */
package Chat;

// socket
import java.net.*;
import java.io.*;

//  Swing
import javax.swing.JTextArea;

//  Crypto
import java.security.*;
import java.security.spec.*;
import java.security.interfaces.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;

public class ChatClientThread extends Thread {

	public static int AUTHENTICATE=0;
	public static int TGTPHASE=1;
	public static int ROOMKEY=2;
	public static int CHAT=3;
	public static int FAILED=4;
	private ChatClient _client;
	private JTextArea _outputArea;
	private Socket _socket = null;
	private int status;

	public ChatClientThread(ChatClient client,int phase) {

		super("ChatClientThread");
		System.out.println("new client thread");
		status=phase;
		_client = client;
		_socket = client.getSocket();
		_outputArea = client.getOutputArea();
	}

	public void run() {

		try {
			if(status==CHAT){
				BufferedReader in = new BufferedReader(
						new InputStreamReader(
								_socket.getInputStream()));

				String msg;

				while ((msg = in.readLine()) != null) {

				     if (msg != null) {
				            _outputArea.append(SymmetricKeyHelper.decryptMessage(msg,SymmetricKeyHelper.readKeyFromFile("room1")));
				        }
				}

				_socket.close();
			}else{
				System.out.println("new client running! :"+status);
				BufferedReader in = new BufferedReader(
						new InputStreamReader(
								_socket.getInputStream()));

				String msg;

				while ((msg = in.readLine()) != null) {

					consumeMessage(msg);
				}

				_socket.close();                
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void consumeMessage(String msg) throws IOException {
		System.out.println("message arrived"+status);
		if(status==AUTHENTICATE){
			handleAuthentication(msg);
		}
		else if(status==TGTPHASE){
			handleTGT(msg);
		}else if(status==ROOMKEY){
			handleRoomKey(msg);
		}
		else if (status == CHAT && msg != null) {
			System.out.println("message!");
			_outputArea.append(">");
			Key roomKey=SymmetricKeyHelper.readKeyFromFile(_client.getRoomKey());
			String plaintext = SymmetricKeyHelper.decryptMessage(msg, roomKey);
			_outputArea.append(plaintext+"\n");
		}

	}
	private void handleAuthentication(String msg){
		System.out.println("Authentication response"+msg.length());
		_client.keepAuthResponse(msg);
	}
	private void handleTGT(String msg){
		System.out.println("TGT geldi g�ya:");
		_client.TGTArrived(msg);

	}
	private void handleRoomKey(String msg){
		System.out.println("Room key:"+msg);
		_client.saveRoomKey(msg);
	}
}
