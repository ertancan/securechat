package Chat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

public class AsymmetricKeyHelper {
	public static String encryptMessage(byte[] message,PublicKey key){
		try{
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] cipherData = cipher.doFinal(message);
			return new String(cipherData);
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}
	public static String encryptMessage(String message,PublicKey key){
		return encryptMessage(message.getBytes(), key);
	}
	public static String decryptMessage(byte[] cipherText,PrivateKey key){
		try{
			Cipher cipher =Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] plainText=cipher.doFinal(cipherText);
			return new String(plainText);
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}
	public static String decryptMessage(String cipherText,PrivateKey key){
		return decryptMessage(cipherText.getBytes(), key);
	}
	public static void generateKeyPairAndWriteToFile(String filename){
		try{
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp = kpg.genKeyPair();
			KeyFactory fact = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec pub = fact.getKeySpec(kp.getPublic(),
					RSAPublicKeySpec.class);
			RSAPrivateKeySpec priv = fact.getKeySpec(kp.getPrivate(),
					RSAPrivateKeySpec.class);

			saveToFile(filename+".pub", pub.getModulus(),
					pub.getPublicExponent());
			saveToFile(filename+".key", priv.getModulus(),
					priv.getPrivateExponent());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void saveToFile(String fileName,
			BigInteger mod, BigInteger exp) throws IOException {
		ObjectOutputStream oout = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(fileName)));
		try {
			oout.writeObject(mod);
			oout.writeObject(exp);
		} catch (Exception e) {
			throw new IOException("Unexpected error", e);
		} finally {
			oout.close();
		}
	}
	public static PrivateKey readPrivateKeyFromFile(String filename){
		ObjectInputStream oin;
		System.out.println("Read private key");
		try {
			InputStream in =new FileInputStream(filename+".key");
			oin =
				new ObjectInputStream(new BufferedInputStream(in));
			BigInteger m = (BigInteger) oin.readObject();
			BigInteger e = (BigInteger) oin.readObject();
			RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			PrivateKey key = fact.generatePrivate(keySpec);
			return key;
		} catch (Exception e) {
			throw new RuntimeException("Spurious serialisation error", e);
		}
	}
	public static PublicKey readPublicKeyFromFile(String filename){
		ObjectInputStream oin;
		try {
			InputStream in =new FileInputStream(filename+".pub");
			oin =
				new ObjectInputStream(new BufferedInputStream(in));
			BigInteger m = (BigInteger) oin.readObject();
			BigInteger e = (BigInteger) oin.readObject();
			RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			PublicKey key = fact.generatePublic(keySpec);
			return key;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Spurious serialisation error", e);
		}
	}
}