//  ChatClient.java
//
//  Modified 1/30/2000 by Alan Frindell
//  Last modified 2/18/2003 by Ting Zhang 
//  Last modified : Priyank Patel <pkpatel@cs.stanford.edu>
//
//  Chat Client starter application.
package Chat;

//  AWT/Swing
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//  Java
import java.io.*;
import java.math.BigInteger;

// socket
import java.net.*;
import java.io.*;
import java.net.*;



//  Crypto
import java.security.*;
import java.security.cert.*;
import java.security.spec.*;
import java.security.interfaces.*;
import java.util.Calendar;
import java.util.Random;

import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;
import javax.security.auth.x500.*;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class ChatClient {

    public static final int SUCCESS = 0;
    public static final int CONNECTION_REFUSED = 1;
    public static final int BAD_HOST = 2;
    public static final int ERROR = 3;
    ChatServer _server;
    ChatClientThread _thread;
    ChatLoginPanel _loginPanel;
    ChatRoomPanel _chatPanel;
    PrintWriter _out = null;
    BufferedReader _in = null;
    CardLayout _layout;
    JFrame _appFrame;
    Socket _socket = null;
    SecureRandom secureRandom;
    KeyStore clientKeyStore;
    KeyStore caKeyStore;
    
    //Info
    String loginName;
    char[] password;
    String keyStoreName;
    char[] keyStorePassword;
    String serverHost;
    int serverPort;
    String authHost;
    int authPort;
    String tgtHost;
    int tgtPort;
    
    String TGT="TGT";
    String authResponse="AUTH";
    String roomKey="error";
//    KeyManagerFactory keyManagerFactory;
//    TrustManagerFactory trustManagerFactory;
  
    //  ChatClient Constructor
    //
    //  empty, as you can see.
    public ChatClient() {

        loginName = null;
        _server = null;

        try {
            initComponents();
        } catch (Exception e) {
            System.out.println("ChatClient error: " + e.getMessage());
            e.printStackTrace();
        }

        _layout.show(_appFrame.getContentPane(), "Login");

    }

    public void run() {
        _appFrame.pack();
        _appFrame.setVisible(true);

    }

    //  main
    //
    //  Construct the app inside a frame, in the center of the screen
    public static void main(String[] args) {
        
        ChatClient app = new ChatClient();

        app.run();
    }

    //  initComponents
    //
    //  Component initialization
    private void initComponents() throws Exception {

        _appFrame = new JFrame("CS255 Chat");
        _layout = new CardLayout();
        _appFrame.getContentPane().setLayout(_layout);
        _loginPanel = new ChatLoginPanel(this);
        _chatPanel = new ChatRoomPanel(this);
        _appFrame.getContentPane().add(_loginPanel, "Login");
        _appFrame.getContentPane().add(_chatPanel, "ChatRoom");
        _appFrame.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                quit();
            }
        });
    }

    //  quit
    //
    //  Called when the application is about to quit.
    public void quit() {

        try {
            _socket.shutdownOutput();
            _thread.join();
            _socket.close();

        } catch (Exception err) {
            System.out.println("ChatClient error: " + err.getMessage());
            err.printStackTrace();
        }

        System.exit(0);
    }
    public void saveInfo(String _loginName, char[] _password,
            String _keyStoreName, char[] _keyStorePassword,
            String _serverHost, int _serverPort,String _authHost, int _authPort,
            String _tgtHost, int _tgtPort){
    	loginName=_loginName;
    	password=_password;
    	keyStoreName=_keyStoreName;
    	keyStorePassword=_keyStorePassword;
    	serverHost=_serverHost;
    	serverPort=_serverPort;
    	authHost=_authHost;
    	authPort=_authPort;
    	tgtHost=_tgtHost;
    	tgtPort=_tgtPort;
    }
    //
    //  connect
    //
    //  Called from the login panel when the user clicks the "connect"
    //  button. You will need to modify this method to add certificate
    //  authentication.  
    //  There are two passwords : the keystorepassword is the password
    //  to access your private key on the file system
    //  The other is your authentication password on the CA.
    //
    public int connect() {

        try {

            //
            //  Read the client keystore
            //         (for its private/public keys)
            //  Establish secure connection to the CA
            //  Send public key and get back certificate
            //  Use certificate to establish secure connection with server
            //

            _socket = new Socket(serverHost, serverPort);
            _out = new PrintWriter(_socket.getOutputStream(), true);

            _in = new BufferedReader(new InputStreamReader(
                    _socket.getInputStream()));

            _layout.show(_appFrame.getContentPane(), "ChatRoom");

            _thread = new ChatClientThread(this,ChatClientThread.CHAT);
            _thread.start();
            return SUCCESS;

        } catch (UnknownHostException e) {

            System.err.println("Don't know about the serverHost: " + serverHost);
            System.exit(1);

        } catch (IOException e) {

            System.err.println("Couldn't get I/O for "
                    + "the connection to the serverHost: " + serverHost);
            System.out.println("ChatClient error: " + e.getMessage());
            e.printStackTrace();

            System.exit(1);

        } catch (AccessControlException e) {

            return BAD_HOST;

        } catch (Exception e) {

            System.out.println("ChatClient err: " + e.getMessage());
            e.printStackTrace();
        }

        return ERROR;

    }
    
    public int authenticate(){
        try {
        	_socket = new Socket(authHost, authPort);
            _out = new PrintWriter(_socket.getOutputStream(), true);
            _in = new BufferedReader(new InputStreamReader(
                    _socket.getInputStream()));
            PublicKey serverPub=AsymmetricKeyHelper.readPublicKeyFromFile("server");
            Key symmetricKey=SymmetricKeyHelper.readKeyFromFile(loginName);
            Calendar cal=Calendar.getInstance();
            Random r=new Random(cal.getTimeInMillis());
            String plainText=r.nextInt(99999)+"::"+loginName+"::"+SymmetricKeyHelper.encryptMessage(""+(cal.getTimeInMillis()+	10000), symmetricKey);
            String cipherText=AsymmetricKeyHelper.encryptMessage(plainText, serverPub);
            String encoded=new BASE64Encoder().encode(cipherText.getBytes());
            encoded=encoded.replaceAll("\n", "");
            //System.out.println("cipher bu:"+cipherText);
            String hello=_in.readLine();
            System.out.println(encoded);
            _out.println(encoded);
            
            _thread = new ChatClientThread(this,ChatClientThread.AUTHENTICATE);
            _thread.start();
            return SUCCESS;


        } catch (UnknownHostException e) {

            System.err.println("Don't know about the serverHost: " + authHost);
            System.exit(1);

        } catch (IOException e) {

            System.err.println("Couldn't get I/O for "
                    + "the connection to the serverHost: " + authHost);
            System.out.println("ChatClient error: " + e.getMessage());
            e.printStackTrace();

            System.exit(1);

        } catch (AccessControlException e) {

            return BAD_HOST;

        } catch (Exception e) {

            System.out.println("ChatClient err: " + e.getMessage());
            e.printStackTrace();
        }

        return ERROR;
    }

    public int requestTGT(){
    	 try {
             //
             //  Read the client keystore
             //         (for its private/public keys)
             //  Establish secure connection to the CA
             //  Send public key and get back certificate
             //  Use certificate to establish secure connection with server
             //

    	     _socket = new Socket(tgtHost, tgtPort);
             _out = new PrintWriter(_socket.getOutputStream(), true);
             _in = new BufferedReader(new InputStreamReader(
                     _socket.getInputStream()));
             PublicKey serverPub=AsymmetricKeyHelper.readPublicKeyFromFile("server");
             String cipherText=AsymmetricKeyHelper.encryptMessage(authResponse, serverPub);
             String encoded=new BASE64Encoder().encode(cipherText.getBytes());
             encoded=encoded.replaceAll("\n", "");
             //System.out.println("cipher bu:"+cipherText);
             String hello=_in.readLine();
             System.out.println(encoded);
             _out.println(encoded);
             
             _thread = new ChatClientThread(this,ChatClientThread.TGTPHASE);
             _thread.start();
             return SUCCESS;

         } catch (UnknownHostException e) {

             System.err.println("Don't know about the serverHost: " + authHost);
             System.exit(1);

         } catch (IOException e) {

             System.err.println("Couldn't get I/O for "
                     + "the connection to the serverHost: " + authHost);
             System.out.println("ChatClient error: " + e.getMessage());
             e.printStackTrace();

             System.exit(1);

         } catch (AccessControlException e) {

             return BAD_HOST;

         } catch (Exception e) {

             System.out.println("ChatClient err: " + e.getMessage());
             e.printStackTrace();
         }

         return ERROR;
    }

    public int TGTArrived(String TGT){
    	this.TGT=TGT;
    	  try {

              //
              //  Read the client keystore
              //         (for its private/public keys)
              //  Establish secure connection to the CA
              //  Send public key and get back certificate
              //  Use certificate to establish secure connection with server
              //
    		  _socket = new Socket(tgtHost, tgtPort);
    		  _out = new PrintWriter(_socket.getOutputStream(), true);
              _in = new BufferedReader(new InputStreamReader(
                      _socket.getInputStream()));
              PublicKey serverPub=AsymmetricKeyHelper.readPublicKeyFromFile("server");
              String cipherText=AsymmetricKeyHelper.encryptMessage(loginName+"::"+TGT, serverPub);
              String encoded=new BASE64Encoder().encode(cipherText.getBytes());
              encoded=encoded.replaceAll("\n", "");
              //System.out.println("cipher bu:"+cipherText);
              String hello=_in.readLine();
              System.out.println(encoded);
              _out.println("request=1::"+encoded);
              
              _thread = new ChatClientThread(this,ChatClientThread.ROOMKEY);
              _thread.start();
              return SUCCESS;

          } catch (UnknownHostException e) {

              System.err.println("Don't know about the serverHost: " + serverHost);
              System.exit(1);

          } catch (IOException e) {

              System.err.println("Couldn't get I/O for "
                      + "the connection to the serverHost: " + serverHost);
              System.out.println("ChatClient error: " + e.getMessage());
              e.printStackTrace();

              System.exit(1);

          } catch (AccessControlException e) {

              return BAD_HOST;

          } catch (Exception e) {

              System.out.println("ChatClient err: " + e.getMessage());
              e.printStackTrace();
          }

          return ERROR;
    }
    //  sendMessage
    //
    //  Called from the ChatPanel when the user types a carrige return.
    public void sendMessage(String msg) {

        try {

        	 //ENCRYPTION - Commited
            Key roomKey=SymmetricKeyHelper.readKeyFromFile(this.roomKey);
            String ciphertext = SymmetricKeyHelper.encryptMessage(loginName + "> " +msg, roomKey);
            _out.println(ciphertext);
            
             
        } catch (Exception e) {

            System.out.println("ChatClient err: " + e.getMessage());
            e.printStackTrace();
        }

    }

    public Socket getSocket() {

        return _socket;
    }

    public JTextArea getOutputArea() {

        return _chatPanel.getOutputArea();
    }
    public void keepAuthResponse(String response){
    	try {
    		System.out.println(response);
			authResponse=AsymmetricKeyHelper.decryptMessage(new BASE64Decoder().decodeBuffer(response), AsymmetricKeyHelper.readPrivateKeyFromFile(loginName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	requestTGT();
    }
    public void saveRoomKey(String keyName){
    	roomKey=keyName;
    	connect();
    }
    public String getRoomKey(){
    	return roomKey;
    }
}
