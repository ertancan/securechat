//
//  AuthServerThread.java
//
//  Written by : Priyank Patel <pkpatel@cs.stanford.edu>
//
//  Accepts connection requests and processes them
package Chat;

// socket
import java.net.*;
import java.io.*;

// Swing
import javax.swing.JTextArea;

//  Crypto
import java.security.*;
import java.security.spec.*;
import java.security.interfaces.*;
import java.util.Calendar;

import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class AuthServerThread extends Thread {

    private AuthServer _as;
    private ServerSocket _serverSocket = null;
    private int _portNum;
    private String _hostName;
    private JTextArea _outputArea;

    public AuthServerThread(AuthServer as) {

        super("AuthServerThread");
        _as = as;
        _portNum = as.getPortNumber();
        _outputArea = as.getOutputArea();
        _serverSocket = null;

        try {

            InetAddress serverAddr = InetAddress.getByName(null);
            _hostName = serverAddr.getHostName();

        } catch (UnknownHostException e) {
            _hostName = "0.0.0.0";
        }
    }
    
    //  Accept connections and service them one at a time
    public void run() {
        
            try {
				_serverSocket = new ServerSocket(_portNum);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            _outputArea.append("AS waiting on " + _hostName + " port " + _portNum);
            while (true) {
            	try {
                Socket socket = _serverSocket.accept();
                PrintWriter _out=new PrintWriter(socket.getOutputStream(),true);
                BufferedReader _in=new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                _out.println("Hello");
                String input=_in.readLine();
                System.out.println(input);
                PrivateKey serverPrivate=AsymmetricKeyHelper.readPrivateKeyFromFile("server");
                String dec=AsymmetricKeyHelper.decryptMessage(new BASE64Decoder().decodeBuffer(input), serverPrivate);
                System.out.println("dekrip:"+dec);
                String[] parts=dec.split("::");
                String username=parts[1];
                Key userPrivateKey=SymmetricKeyHelper.readKeyFromFile(username);
                String challangeResponse=parts[2];
                String resp=SymmetricKeyHelper.decryptMessage(challangeResponse, userPrivateKey);
                long time=Long.parseLong(resp);
                Calendar cal=Calendar.getInstance();
                if(time>cal.getTimeInMillis()-100000){
                	 Key serverSym=SymmetricKeyHelper.readKeyFromFile("server");
                     String serverSecretUserText=SymmetricKeyHelper.encryptMessage("boyle random padding gorulmedi::"+username, serverSym);
                     PublicKey userPub=AsymmetricKeyHelper.readPublicKeyFromFile(username);
                     String cipherText=AsymmetricKeyHelper.encryptMessage(serverSecretUserText, userPub);
                     _outputArea.append("Response Arrived\n");
                     String encoded=new BASE64Encoder().encode(cipherText.getBytes());
                      _out.println(encoded.replace("\n", ""));
                }else{
                	System.out.println("Fail!");
                	
                }
               _out.close();
               _in.close();
               socket.close();
            	 } catch (Exception e) {
                     //System.out.println("AS thread error: " + e.getMessage());
                     e.printStackTrace();
                     _outputArea.append("Exception Occured\n");
                 }
                //
                //  Got the connection, now do what is required
                //  
            }
       

    }
}
